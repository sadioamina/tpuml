# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 14:35:40 2020

@author: hp
"""

class Etudiant:

	def __init__(self,nom,prenom,numTel,email,dateEntreUniv,note):
         self.__nom = nom
         self.__prenom = prenom
         self.__dateEntreUniv = dateEntreUniv
         self.__numTel = numTel
         self.__email = email
         self.__note = {}

	def __str__(self):
 		return 'nom:' + str(self.__nom) + ' ; prenom:' + str(self.__prenom) +' ; dateEntreUniv:' + str(self.__dateEntreUniv) + ' ; numTel:' + str(self.__numTel) + ' ; email:' + str(self.__email)

	def setNote(self,prenom,matiere,note1,note2,note3): 
         self.__note[prenom] = note1,note2,note3
         


	def getNote(self,prenom):
 		return 'Note ' +  str(prenom) + ': ' +  str(self.__note[prenom])

	def getMoyenne(self,prenom):
         return sum(self.__note.values())/len(self.__note.values())

         
         


if __name__=='__main__':
    eleve= Etudiant('hugo','jean','0769713943','h.jean@gmail.com','2017','10')
    #print(eleve)
    eleve.setNote('amina','inlo','12','13','16')
    eleve.setNote('lina','cag','10','09','18')
    eleve.setNote('hugo','programmation','18','16','14')
    print(eleve.getNote('amina'))
    #print(eleve.getNote('hugo'))
    #print(eleve.getNote('lina'))
    #print(eleve.getMoyenne('amina'))
    #print(eleve.getMoyenne('amina'))

'''
#CAS D'UTILISATION ET DIAGRAMME
#1

# Rechercher le nom d'une université 

# Afficher les informations d'un enseignant ( numero de téléphone,date de prise de #fonction,nom,prénom, email,matière enseignée)

# Afficher toutes les informations d'un étudiant : numero de tel, email,
#date d'arrivée
# Afficher les matières que font un étudiant

# Afficher tous les departements d'enseignement de chaque  université
# Afficher tous les enseignants d'un departement d'enseignement 

# Afficher les notes par enseignement 
# Affiche les notes par matière

 #Afficher les enseignants d'une matiere 
'''

'''
cas d'utilisation : afficher les informations d'un enseignant

Description :l'utilisateur entre le nom de l'enseignant dans le site de l'université
Acteur: l'utilisateur

Scénariode base : - l'utilisateur entre le nom et le prénom de l'enseignant
                  - l'utilisateur renseigne la matière enseignée par l'enseignant
                  - le site va retourner les informations correspondantes
                  
Contraintes: l'enseignant doit enseigner au sein de l'université dans la méme année d'étude

'''


    
    
    
    
    
